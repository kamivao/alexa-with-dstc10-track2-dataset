# <snippet_imports>
import os
import json
import itertools
from task2.scripts.kendalls_rankings_preproc import *
from collections import defaultdict
# </snippet_imports>
# Created by Debanjan Chaudhuri at 9/22/2021

model_outputs_f = '../data/dstc10_test/'
output_ensemble_f = '../data/c_labels_system_6.json'
knowledge_f = '../data/knowledge_relation.json'
outputs = []
for out_f in os.listdir(model_outputs_f):
    with open(model_outputs_f+out_f) as f:
        out = json.load(f)
        outputs.append(out)
ensemble_file = []
with open(knowledge_f, 'r') as f:
    kg = json.load(f)

ent2domain = defaultdict()
for d, dv in kg.items():
    for e_id, v in dv.items():
        ent2domain[e_id] = d


def most_common(lst):
    return max(set(lst), key=lst.count)


for k, d in enumerate(outputs[0]):
    # print (k)
    if d['target']:
        esb_out = defaultdict(dict)
        esb_out['target'] = d['target']
        pred_entities = []
        pred_doc = []  # for relations
        pred_response = []
        pred_domains = []
        pred_entities.append(d['knowledge'][0]['entity_id']) # get for the first file
        rel = [d['knowledge'][j]['doc_id'] for j in range(len(d['knowledge']))]
        pred_doc.append([d['knowledge'][0]['entity_id'], rel])
        pred_response.append([d['knowledge'][0]['entity_id'], d['response']])
        pred_domains.append([d['knowledge'][0]['entity_id'], d['knowledge'][0]['domain']])
        for o in outputs[1:]:  # get for the other files
            ent = o[k]['knowledge'][0]['entity_id']
            resp = o[k]['response']
            rel_pred = [o[k]['knowledge'][j]['doc_id'] for j in range(len(o[k]['knowledge']))]
            pred_entities.append(ent)
            pred_doc.append([ent, rel_pred])
            pred_response.append([ent, resp])
            pred_domains.append([ent, o[k]['knowledge'][0]['domain']])
        # print (pred_entities)
        # print (pred_doc) #
        if len(set(pred_entities)) == 1:  # all entities are same
            ent_vote = pred_entities[0]
        else:
            ent_vote = most_common(pred_entities)
            # print (pred_entities)
            # print (ent_vote)
        out_dict = []
        relation_prob_list = [r for e, r in pred_doc if e == ent_vote]
        # print (relation_prob_list)
        relation_prob_list.sort()
        unique_relations = list(k for k, _ in itertools.groupby(relation_prob_list))  # get unique lists for voted ent
        # print (unique_relations)
        # print (len(unique_relations))
        # prob_domain = [dom for e, dom in pred_domains if e == ent_vote]
        # best_domain = most_common(prob_domain)
        ent_domain = ent2domain[str(ent_vote)]
        if len(unique_relations) == 1:
            for r in unique_relations[0]:
                out = {}
                out['domain'] = ent_domain
                out['entity_id'] = ent_vote
                out['doc_id'] = r
                out_dict.append(out)
        else:
            rankings = convert_ids_to_ranks(unique_relations)
            W = kendall_w(np.array(rankings))
            if W < 0.6:
                shuffled_doc_ids = rankings_shuffle(unique_relations)
            else:
                shuffled_doc_ids = unique_relations[0]
            for r in shuffled_doc_ids:
                out = {}
                out['domain'] = ent_domain
                out['entity_id'] = ent_vote
                out['doc_id'] = r
                out_dict.append(out)

        esb_out['knowledge'] = out_dict
        # unique_response = [resp for e, resp in pred_response if e == ent_vote]

        # ensemble_response = most_common(unique_response)
        best_doc = out_dict[0]['doc_id']
        response_out = kg[ent_domain][str(ent_vote)]['docs'][str(best_doc)]['body']
        esb_out['response'] = response_out
        ensemble_file.append(esb_out)
    else:
        ensemble_file.append(d)

print (ensemble_file[86])
with open(output_ensemble_f, 'w') as f:
    json.dump(ensemble_file, f, indent=4)


