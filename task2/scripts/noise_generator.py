import pandas as pd
import codecs, os
import json
from numpy.random import randint
from numpy.random import seed
import nltk
from nltk import FreqDist
from collections import OrderedDict
from itertools import islice
import string
import random

lower_upper_alphabet = string.ascii_letters
high_frequency_tokens = ['i', 'the', 'to', 'a', 'for', 'you', 'that', 'can', 'need', 'and', 'is', 'in', 'me', 'on',
 'train', 'have', 'please', 'do', 'be', 'does', 'it', 'my', 'yes', 'restaurant', 'hotel', 'at',
 'there', 'of', 'will', 'thank', 'would', 'like', 'if', 'looking', 'also', 'with', 'book', 'all',
 'what', 'am', 'this', 'they', 'cambridge', 'no', "'m", "'s", 'number', 'help', 'from', 'free',
 'people', 'get', 'thanks', 'parking', 'taxi', 'wifi', 'any', 'are', 'place', 'by', 'one', 'find',
 'great', 'how', 'time', 'about', 'price', 'offer', 'could', 'booking', 'town', 'tell', 'want', "n't",
 'stay', 'food', 'know', 'area', 'centre', 'as', 'nights', 'leave', 'just', 'address', 'an', 'go',
 'phone', "'d", '4', 'your', 'much', 'fee', 'ticket', 'after', 'so', 'range', 'station', 'pay',
 'reference', 'postcode', 'not', 'should', 'able', "'ll", 'arrive', 'expensive', 'but', 'later',
 '3', 'change', 'sounds', 'guesthouse', 'reservation', 'today', 'leaving', 'give', 'good', 'star',
 'service', 'their', 'available', 'starting', 'same', 'house', 'going', 'information', 'make', '2',
 'may', 'options', 'needed', 'or', '5', 'bike', 'cancel', 'table', 'day', 'some', 'sunday', 'saturday',
 'room', 'travel', 'friday', 'goodbye', 'cheap', 'allow', 'has', 'tuesday', 'north', 'type', 'possible',
 'thursday', 'monday', 'bring', 'take', 'wednesday', 'sure', 'well', 'something', 'we', 'okay', 'guests',
 'provide', 'use', 'now', 'entrance', 'attraction', 'fine', 'actually', 'try', 'tickets', 'moderate',
 'departing', 'let', 'places', 'park', 'serves', 'car', 'priced', 'west', 'east', 'serve', 'called',
 'children', 'check', 'recommend', 'first', 'guest', 'vegetarian', 'moderately', 'smoking', '8', '7', '6', 'bye',
 'reserve', 'hi', 'was', 'ok', 'facilities', 'really', 'think', 'everything', 'charge', 'south', 'college',
 'seating', 'museum', 'part', 'card', 'accept', 'allowed', 'access', 'too', 'london', 'street', 'outdoor', 'kings',
 'ride', 'very', 'live', 'matter', 'plans', 'out', 'eat', '1', 'music', 'onsite', 'gluten', 'visa', 'visit', 'disability',
 'alcohol', 'customers', 'care', 'center', 'confirmation', 'traveling', 'long', 'two', 'stars', 'internet', 'services',
 'restaurants', 'whether', 'byob', 'bed', 'rating', 'delivery', 'side', 'hello', 'indian', 'mastercard', 'seat',
 'perfect', 'anything', 'cost', 'leaves', 'prefer', 'then', 'when', 'depart', 'chinese', 'written', 'breakfast',
 'city', 'deposit', 'chairs', 'amex', 'more', 'nice', 'menu', 'instead', 'wheelchair', 'trip', 'those', 'them',
 'season', 'data', 'open', 'extra', 'where', 'yet', 'work', 'contact', 'dine', 'up', 'pool', 'located', 'between',
 'credit', 'before', 'bar', 'id', 'hours', 'include', 'rooms', 'departure', 'preference']


def add_fillers(sentence):
    fillers = ['um', 'umm', 'er', 'uh', 'uhh', 'hmm', 'kinda', 'em well', 'em']
    tokens = sentence.split(" ")

    values = randint(0, len(tokens)-1, int(round(0.1 * len(tokens)))) # generate indices where to insert fillers depending on the length of the utterance

    for val in values:
        filler_index = randint(0, len(fillers), 1)
        filler = fillers[filler_index[0]]
        tokens.insert(val, filler)
    updated_string = ' '.join(tokens)
    return updated_string


def gen_freq_dist(list_of_strings):
    corpus = ' '.join(list_of_strings)
    tokens = nltk.word_tokenize(corpus)
    fdist = FreqDist(tokens)
    sorted_fdist = sorted(fdist.items(), key=lambda kv: kv[1], reverse=True)

    freq_dict = OrderedDict(sorted_fdist)
    sliced = islice(freq_dict.items(), 299)  # o.iteritems() is o.items() in Python 3
    sliced_o = OrderedDict(sliced)
    high_frequency_tokens = []
    for key, val in sliced_o.items():
        high_frequency_tokens.append(key)
    print(high_frequency_tokens)
    return high_frequency_tokens

def corrupt_high_frequency_tokens(hf_tokens_list, sentence, counter):
    tokens = sentence.split(" ")

    for token in tokens:
        if token in high_frequency_tokens:
            # if the number is odd, do operations for the token
            integer = randint(2000)
            if integer % 2 == 1:
                # replace letter in tokens longer than 4
                if counter % 8 == 0 and len(token) > 3:
                    try:
                        random_index = randint(0, len(token), 1)
                        random_letter = random.choice(lower_upper_alphabet)
                        broken_token = token.replace(token[random_index[0]], random_letter.lower(), 1)
                        print("Replace: ", broken_token)
                        tokens = [broken_token if x == token else x for x in tokens]
                        flag = False
                    except Exception as ex:
                        continue

                # drop letter in short tokens, 2-3
                elif counter % 8 == 0 and len(token) <= 3:
                    try:
                        broken_token = token[:-1]
                        tokens = [broken_token if x == token else x for x in tokens]
                        print("Cut: ", broken_token)
                    except Exception as ex:
                        continue

            elif integer % 2 == 0:
                # split word longer than 6 and replace a letter in the 2 nd piece
                if counter % 6 == 0 and len(token) > 8:
                    try:
                        a = token[:4]
                        b = token[5:]
                        random_index = randint(0, len(b), 1)
                        random_letter = random.choice(lower_upper_alphabet)
                        broken_b = token.replace(token[random_index[0]], random_letter.lower(), 1)
                        i = tokens.index(token)  # This raises ValueError if there's no 'b' in the list.
                        tokens[i:i + 1] = a, broken_b
                        print("Split with replacement: ", a, broken_b)
                    except Exception as ex:
                        continue

    updated_string = ' '.join(tokens)
    return updated_string


#dataroot = '../data/entity_classification'
dataroot = '../data/ks_turn_detection'
path = os.path.join(os.path.abspath(dataroot))


col_names = ['sentence', 'label']
train_dataset = pd.read_csv(os.path.join(path, 'ks_turn_detection_train_all.csv'), names=col_names, header=0)

texts = train_dataset['sentence'].tolist()
labels = train_dataset['label'].tolist()


broken_texts = []
#sample = texts[100:200]
for i in range(0, len(texts)):
    item = texts[i]
    print(item)
    sent = add_fillers(item)
    #corrupted_sent = corrupt_high_frequency_tokens(hf_tokens_list=high_frequency_tokens, sentence=sent, counter=i)
    broken_texts.append(sent)
    #print(corrupted_sent)

dataset = pd.DataFrame(
    {'sentence': broken_texts,
     'labels': labels
    })

dataset = dataset.sample(frac=1)
dataset.to_csv(os.path.join(path, 'ks_turn_detection_train_all_hes.csv'), index=False)


