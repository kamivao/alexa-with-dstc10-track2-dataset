import scipy.stats as stats
# takes 2 lists with document ids and return ranks for each of them

import numpy as np
import random

def kendall_w(expt_ratings):
    if expt_ratings.ndim!=2:
        print("ratings matrix must be 2-dimensional")
    m = expt_ratings.shape[0] #raters
    n = expt_ratings.shape[1] # items rated
    denom = m**2*(n**3-n)
    rating_sums = np.sum(expt_ratings, axis=0)
    S = n*np.var(rating_sums)
    return 12*S/denom




def convert_ids_to_ranks(list_of_rankings):
    dict = {}
    x = []
    for list in list_of_rankings:
        for item in list:
            x.append(item)

    keys = set(x)
    # print(keys)

    for key in keys:
        dict[key] = []

        for list in list_of_rankings:
            if key in list:
                dict[key].insert(list_of_rankings.index(list), list.index(key)+1)
            else:
                dict[key].insert(list_of_rankings.index(list), 6)


    # print(dict)
    output = []
    n = len(list_of_rankings)
    for i in range(0, n):
        ranking = []
        for k, v in dict.items():
            ranking.append(v[i])
        # print(ranking)
        output.append(ranking)

    return(output)

def rankings_shuffle(list_of_rankings):

    x = []
    for l in list_of_rankings:
        for item in l:
            x.append(item)

    keys = set(x)
    ids = list(keys)
    # print(ids)
    random.shuffle(ids)
    if len(list_of_rankings[0]) >= 5:
        shuffled_ranking = ids[0:5]
    else:

        shuffled_ranking = ids[0:len(list_of_rankings[0])]

    return shuffled_ranking

# pred_ids = [[9, 13, 0, 4, 5], [9, 15, 13, 4, 5], [34, 13, 0, 4, 5]]
# rankings = convert_ids_to_ranks(pred_ids)
# W = kendall_w(np.array(rankings))
# if W < 0.6:
#     shuffled_doc_ids = rankings_shuffle(pred_ids)
#
# print(W, shuffled_doc_ids)


