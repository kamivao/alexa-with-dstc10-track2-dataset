import json
import os

dname = os.path.dirname(os.getcwd())
os.chdir(dname)

def top_one(labels):
    for label in labels:
        if label["target"] == True:
            doc_ids = label["knowledge"][0]["doc_id"]
            # int_doc_ids = []
            for id in doc_ids:
                # int_id = int(id)
                # int_doc_ids.append(int_id)
                label["knowledge"][0]["doc_id"] = int(doc_ids[0])
    return labels

# python scripts/check_results.py --dataset test --dataroot data/dstc10_test --outfile data/dstc10_test/c_labels_system_6.json
# python scripts/scores.py --dataset val --dataroot data/ --outfile data/c_val_system_5_gpt2.json --scorefile data/val/scores_system_5_gpt2.json
with open("data/dstc10_test/test/system_13_test_set.json", 'rb') as fl3:
    labels = json.load(fl3)
    for label in labels:
        if label["target"] == True:
            kn = []
            doc_ids = label["knowledge"][0]["doc_id"]
            print(doc_ids)
            for id in doc_ids:
                snippet = {"domain": label["knowledge"][0]["domain"], "entity_id": label["knowledge"][0]["entity_id"]}
                print(id)
                snippet["doc_id"] = int(id)
                print(snippet)
                kn.append(snippet)
                print(kn)
                snippet = {}
            label["knowledge"] = kn

with open("data/dstc10_test/test/c_system_13_test_set.json", 'w') as outfile:
        json.dump(labels, outfile, indent=2)

#data/dstc10_test/c_labels_system_5.json