import codecs
from fuzzywuzzy import fuzz
import json
import numpy as np
#from task2.scripts.extract_kg import create_kg  # kg creator
from task2.scripts.relation_predict import predict_relation  # relation predictor
import os
import torch
from tqdm import tqdm

def load_model(architecture, task):
    if architecture == 'roberta' and task == 'detection':
        from transformers import RobertaForSequenceClassification, RobertaTokenizer
        model = RobertaForSequenceClassification.from_pretrained("../models/ks_turn_detection/roberta")
        print("Loaded model: ", model)
        tokenizer = RobertaTokenizer.from_pretrained("../models/ks_turn_detection/roberta")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    if architecture == 'roberta_asr' and task == 'detection':
        from transformers import RobertaForSequenceClassification, RobertaTokenizer
        model = RobertaForSequenceClassification.from_pretrained("../models/ks_turn_detection/roberta_asr")
        print("Loaded model: ", model)
        tokenizer = RobertaTokenizer.from_pretrained("../models/ks_turn_detection/roberta_asr")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    if architecture == 'roberta_asr_fillers' and task == 'detection':
        from transformers import RobertaForSequenceClassification, RobertaTokenizer
        model = RobertaForSequenceClassification.from_pretrained("../models/ks_turn_detection/roberta_asr_fillers")
        print("Loaded model: ", model)
        tokenizer = RobertaTokenizer.from_pretrained("../models/ks_turn_detection/roberta_asr_fillers")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    elif architecture == 'bert' and task == 'selection':
        from transformers import AutoModelForSequenceClassification, AutoTokenizer
        model = AutoModelForSequenceClassification.from_pretrained("../models/ent_clf_task2")
        print("Loaded model: ", model)
        tokenizer = AutoTokenizer.from_pretrained("../models/ent_clf_task2")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    elif architecture == 'gpt2' and task == 'selection':
        from transformers import GPT2ForSequenceClassification, GPT2Tokenizer
        model = GPT2ForSequenceClassification.from_pretrained("../models/ent_clf_gpt2_seq_800_hde")

        tokenizer = GPT2Tokenizer.from_pretrained("../models/ent_clf_gpt2_seq_800_hde")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    elif architecture == 'longformer' and task == 'selection':
        from transformers import LongformerForSequenceClassification, LongformerTokenizer
        tokenizer = LongformerTokenizer.from_pretrained("../models/ent_clf_longformer_900")
        print("Loaded tokenizer: ", tokenizer)
        model = LongformerForSequenceClassification.from_pretrained("../models/ent_clf_longformer_900")
        print("Loaded model: ", model)
        return model, tokenizer

    elif architecture == 'gpt2_fillers' and task == 'selection':
        from transformers import GPT2ForSequenceClassification, GPT2Tokenizer
        model = GPT2ForSequenceClassification.from_pretrained("../models/gpt2_es_train5_long_fillers")

        tokenizer = GPT2Tokenizer.from_pretrained("../models/gpt2_es_train5_long_fillers")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    elif architecture == 'gpt2_large_noisy' and task == 'selection':
        from transformers import GPT2ForSequenceClassification, GPT2Tokenizer
        model = GPT2ForSequenceClassification.from_pretrained("../models/gpt2_large_noisy")

        tokenizer = GPT2Tokenizer.from_pretrained("../models/gpt2_large_noisy")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    elif architecture == 'roberta_fillers' and task == 'selection':
        from transformers import RobertaForSequenceClassification, RobertaTokenizer
        model = RobertaForSequenceClassification.from_pretrained("../models/roberta_es_5_long_fillers")
        print("Loaded model: ", model)
        tokenizer = RobertaTokenizer.from_pretrained("../models/roberta_es_5_long_fillers")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    elif architecture == 'roberta_large_noisy' and task == 'selection':
        from transformers import RobertaForSequenceClassification, RobertaTokenizer
        model = RobertaForSequenceClassification.from_pretrained("../models/roberta_large_noisy")
        print("Loaded model: ", model)
        tokenizer = RobertaTokenizer.from_pretrained("../models/roberta_large_noisy")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    elif architecture == 'transformerxl' and task == 'domain_classification':
        from transformers import TransfoXLForSequenceClassification, TransfoXLTokenizer
        model = TransfoXLForSequenceClassification.from_pretrained("../models/domain_clf/transformerxl")
        tokenizer = TransfoXLTokenizer.from_pretrained("../models/domain_clf/transformerxl")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    elif architecture == 'roberta' and task == 'domain_classification':
        from transformers import RobertaForSequenceClassification, RobertaTokenizer
        model = RobertaForSequenceClassification.from_pretrained("../models/domain_clf/roberta")
        tokenizer = RobertaTokenizer.from_pretrained("../models/domain_clf/roberta")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    elif architecture == 'roberta_large_noisy' and task == 'domain_classification':
        from transformers import RobertaForSequenceClassification, RobertaTokenizer
        model = RobertaForSequenceClassification.from_pretrained("../models/domain_clf/roberta_large_noisy")
        tokenizer = RobertaTokenizer.from_pretrained("../models/domain_clf/roberta_large_noisy")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer


def detect_ks_turn(tokenizer, model, seq):
    try:
        inputs = tokenizer(seq, return_tensors="pt")
        outputs = model(**inputs)
        classification_logits = model(**inputs)[0]
        pred_labels = torch.softmax(classification_logits, dim=1).tolist()[0]
        #print("predicted labels: ", pred_labels)
        scores = pred_labels
        print("ks-turn prediction scores, [0] - false, [1] - true", scores)
        if scores[1] > scores[0]:
            return True
        else:
            return False

    except Exception as ex:
        print(ex)


def find_entities(turn, utterance, knowledge_tracker, knowledge): #, mult_ent are not taken into account
    # print("Plain search has been started for entity retrieval.")
    entity_mentions = []
    candidates_scoring = {}
    entity_meta = {}

    # find the entity
    for dmn in knowledge:
        if dmn in ["hotel", "restaurant", "attraction"]:
            data = knowledge[dmn]
            for key, val in data.items():
                kb_entity = data[key]["name"]
                docs = data[key]["docs"]

                token_set_ratio = fuzz.token_set_ratio(utterance.lower(), kb_entity.lower())
                if token_set_ratio >= 90:
                    candidates_scoring[kb_entity.lower()] = token_set_ratio
                    entity_meta[kb_entity.lower()] = {"domain": dmn, "entity_name": kb_entity, "entity_id": int(key), "docs": docs}
                #print(kb_entity.lower(), token_set_ratio)

    if len(candidates_scoring) > 0:
        if len(candidates_scoring) == 1:
            for key, val in candidates_scoring.items():
                entity_mention = key
            entity_mentions.append(entity_meta[entity_mention])
            knowledge_tracker[turn] = [entity_mentions[0]]
        else:
            # sort entities and prune the list only to entities with ratio == 100
            ranked_candidates_scoring = sorted(candidates_scoring.items(), key=lambda kv: kv[1], reverse=True)
            print(ranked_candidates_scoring)
            full_match = []
            for tuple in ranked_candidates_scoring:
                if tuple[1] >= 90:
                    full_match.append(tuple[0])

            # get longest match, if it exists
            if len(full_match) > 1:
                longest_full_match = []
                for i in range(0, len(full_match)-1):
                    for j in range(i+1, len(full_match)):

                        a = full_match[i]
                        b = full_match[j]
                        print(full_match, i, j, a, b)
                        if a in b:
                            longest_full_match.append(b)
                        elif b in a:
                            longest_full_match.append(a)
                        else:
                            if a not in longest_full_match:
                                longest_full_match.append(a)
                            if b not in longest_full_match:
                                longest_full_match.append(b)

            knowledge_tracker[turn] = []
            for ent in longest_full_match:
                knowledge_tracker[turn].append(entity_meta[ent])
            print(knowledge_tracker[turn])

    else:
        knowledge_tracker[turn] = []


    return knowledge_tracker


def select_entity(tokenizer, model, clf_input):
    try:
        inputs = tokenizer(clf_input, return_tensors="pt")
        #outputs = model(**inputs)
        classification_logits = model(**inputs)[0]
        pred_labels = torch.softmax(classification_logits, dim=1).tolist()[0]
        #print("predicted labels: ", pred_labels)
        score = pred_labels[1]
        return score

    except:
        score = np.random.uniform(0.01, 0.99)
        return score


def select_entity_from_retrieved(tokenizer, model, seq):
    try:
        inputs = tokenizer(seq, return_tensors="pt")
        #outputs = model(**inputs)
        classification_logits = model(**inputs)[0]
        pred_labels = torch.softmax(classification_logits, dim=1).tolist()[0]
        #print("predicted labels: ", pred_labels)
        score = pred_labels[1]
        return score

    except:
        score = np.random.uniform(0.01, 0.99)
        return score


def validate_detected_turn(pred_target, clf_input):
    ks_turn_triggers = ['heat', 'free wifi', 'elevator', 'happy hour', 'noise level', 'do they have t v',
                        'is it a smokin', 'can you check whether they accept', 'good for kid']
    non_ks_turn_triggers = ['confirmation code', 'zip code', 'what type of attraction', 'what type of accomodation']
    if pred_target == True:
        for trigger in non_ks_turn_triggers:
            if trigger in clf_input:
                pred_target = False

    if pred_target == False:
        for trigger in ks_turn_triggers:
            if trigger in clf_input:
                pred_target = True

    return pred_target


def domain_classifier(tokenizer, model, seq):
    try:
        inputs = tokenizer(seq, return_tensors="pt")
        #outputs = model(**inputs)
        classification_logits = model(**inputs)[0]
        pred_labels = torch.softmax(classification_logits, dim=1).tolist()[0]
        #print("predicted labels: ", pred_labels)
        scores = {}
        for i in range(0,5):
            scores[i] = pred_labels[i]
        ranked_scores = sorted(scores.items(), key=lambda kv: kv[1], reverse=True)
        labels = {"hotel": 0, "restaurant": 1, "attraction": 2, "train": 3, "taxi": 4}
        best = ranked_scores[0]
        #print(best)
        for key, val in labels.items():
            #print(key, val)
            if int(best[0]) == labels[key]:
                domain_label = key
                print("Predicted domain: ", domain_label)

        return domain_label

    except Exception as ex:
        print("Exception", ex)
        #score = np.random.uniform(0.01, 0.99)
        domain_label = "hotel"
        return domain_label


if __name__ == "__main__":
    # load models
    #TODO @Debanjan: if you use any new models, add them to the load_model() function

    # load models for detection (subtask 1),  available models: 'roberta'
    ks_turn_detection_model, ks_turn_detection_tokenizer = load_model(architecture='roberta_asr_fillers', task='detection')
    # load models for selection (subtask 2), available models: 'bert', 'gpt2', 'longformer'
    entity_selection_model, entity_selection_tokenizer = load_model(architecture='gpt2_large_noisy', task='selection')
    # load domain classifier
    #domain_clf_model, domain_clf_tokenizer = load_model(architecture='roberta_large_noisy', task='domain_classification')

    mentioned_entities = {}
    focus_entity = ""
    single_entity_domains = ['taxi', 'train']
    multi_entity_domains = ['hotel', 'restaurant', 'attraction']

    dname = os.path.dirname(os.getcwd())
    os.chdir(dname)
    print(dname)

    #with codecs.open("data/knowledge.json", 'rb') as fl:
        #knowledge = json.load(fl)

    with codecs.open("data/kg_rl_old.json", 'rb') as file:
        knowledge_relations = json.load(file)

    with open("data/val/logs.json", 'rb') as fl2:
        logs = json.load(fl2)

    # create the kg Debanjan
    #_kg = create_kg('hotel', 30)  # create kg for hotel returns a dictionary

    #filename = fl2.name
    #with open("data/val/labels.json", 'rb') as fl3:
        #labels = json.load(fl3)

    knowledge_tracker = {}  # dict to track extracted domains and entities for each turn
    true_pos = 0
    detection_accuracy = 0
    n_ks_turns = 0
    errors_detection = []
    errors_selection = []

    output = []


    for i in tqdm(range(0, len(logs))):
        dialogue = logs[i]
        #label = labels[i]

        # start entity retrieval
        for j in range(0, len(dialogue)):
            utterance = dialogue[j]["text"]
            knowledge_tracker = find_entities(utterance=utterance,
                                              knowledge_tracker=knowledge_tracker, knowledge=knowledge_relations, turn=j)


        # # start ks-turn detection
        clf_input = dialogue[-1]["text"]
        pred_target = detect_ks_turn(tokenizer=ks_turn_detection_tokenizer, model=ks_turn_detection_model, seq=clf_input)
        print("Knowledge-seeking turn: ", pred_target)
        # validate the predicted target
        val_target = validate_detected_turn(pred_target=pred_target, clf_input=clf_input)


        if val_target == True: # comment this line and uncomment the next one to run on task 1 labels
            n_ks_turns += 1


            # preprocess dialogue history for entity selection (history + domain + entity name)
            dialogue_history = []
            for turn in dialogue:
                dialogue_history.append(turn["text"])

            # join all different retrieved candidate entities
            candidates = []

            # try first get entities from the last two turns
            last_key = len(knowledge_tracker)-1
            if len(knowledge_tracker[last_key]) > 0:
                knowledge_snippet = knowledge_tracker[last_key][0]

            elif len(knowledge_tracker[last_key-1]) > 0:
                knowledge_snippet = knowledge_tracker[last_key-1][0]

            else:
                for turn, snippet in knowledge_tracker.items():
                    if len(snippet) > 0:
                        for entity_metadata in snippet:
                            if entity_metadata["domain"] in single_entity_domains and entity_metadata[
                                "entity_name"].lower() not in candidates:
                                candidates.append(entity_metadata["domain"])
                            elif entity_metadata["domain"] in multi_entity_domains and entity_metadata[
                                "entity_name"].lower() not in candidates:
                                candidates.append(entity_metadata["entity_name"].lower())
                print("List of candidates: ", candidates)

                # if candidates are empty, make predictions across all extracted entities
                if len(candidates) == 0:
                    for turn, snippet in knowledge_tracker.items():
                        if len(snippet) > 0:
                            for entity_metadata in snippet:
                                if entity_metadata["domain"] in single_entity_domains and entity_metadata[
                                    "entity_name"].lower() not in candidates:
                                    candidates.append(entity_metadata["domain"])
                                elif entity_metadata["domain"] in multi_entity_domains and entity_metadata[
                                    "entity_name"].lower() not in candidates:
                                    candidates.append(entity_metadata["entity_name"].lower())
                print("List of candidates: ", candidates)

                # run entity prediction
                history = ' '.join(dialogue_history)
                # run entity selection
                scores = {}
                for candidate in candidates:
                    clf_input = history + ' ' + candidate
                    input_tokens = clf_input.split(' ')
                    if len(input_tokens) > 510:
                        input_tokens = input_tokens[-510:]
                        clf_input = ' '.join(input_tokens)
                    print(clf_input)
                    score = select_entity(tokenizer=entity_selection_tokenizer, model=entity_selection_model, clf_input=clf_input)
                    scores[candidate] = score

                ranked_scores = sorted(scores.items(), key=lambda kv: kv[1], reverse=True)
                print(ranked_scores)
                if len(ranked_scores) > 0:
                    focus_entity = ranked_scores[0]


                # focus_entity[0] everywhere
                if focus_entity[0] not in single_entity_domains:
                    # print("Focus entity not in single ent domains")
                    for turn, snippet in knowledge_tracker.items():
                        if len(snippet) > 0:
                            for entity_metadata in snippet:
                                if entity_metadata["entity_name"].lower() == focus_entity[0]:
                                    knowledge_snippet = entity_metadata

                # if the prediction is a single entity domain, start document prediction
                if focus_entity[0] in single_entity_domains:
                    data = knowledge_relations[focus_entity[0]]
                    docs = data["*"]["docs"]
                    knowledge_snippet = [{"domain": focus_entity[0], "entity_id": "*", "docs": docs}]
                    knowledge_tracker[j] = knowledge_snippet

            print("KN_SNIPPET: ", knowledge_snippet)
            #if snippet["entity_id"] == ground_ent_id:
                #true_pos += 1
            #else:
                #errors_selection.append(["Truth: ", ground_ent_id, entity_name, ranked_scores, "Predictions: ",
                                         #knowledge_snippet["entity_name"]])

            #TODO @Debanjan here you can start document retrieval using the knowledge snippet
            #docs = knowledge_relations[knowledge_snippet["domain"][str(knowledge_snippet["entity_id"])]["docs"]
            docs = knowledge_snippet["docs"]
            user_question = dialogue_history[-1]
            relations = []
            for doc_id in docs.keys():
                relations.append(docs[doc_id]['relation'])


            doc_pred = predict_relation(query=user_question, relation_set=relations, get_best=False, top_k=5) # 5
            doc_ids_list = []
            for i in range(0, len(doc_pred[0])):
                rel = doc_pred[0][i]
                for doc_id in docs.keys():
                    if docs[doc_id]['relation'] == rel:
                        doc_ids_list.insert(i, int(doc_id))

            answer = docs[str(doc_ids_list[0])]["body"]
            # get best 5
            # updating the output
            # return the answer for the task 3
            #

            #if knowledge_snippet["entity_id"] == ground_ent_id:
                #true_pos += 1
            if len(knowledge_snippet) > 0:
                output.append({"target": True,
                               "knowledge": [
                                   {"domain": knowledge_snippet["domain"],
                                    "entity_id": knowledge_snippet["entity_id"],
                                    "doc_id": doc_ids_list}
                                     ],
                               "response": answer})

            else:
                output.append({"target": True,
                               "knowledge": [
                                   {"domain": "hotel",
                                    "entity_id": 0,
                                    "doc_id": 7
                                    }
                                    ],
                               "response": "Sorry, i don't know the answer for it. Can I help you with something else?"})


            knowledge_tracker = {}
        else:
            output.append({"target": False})
    with open("data/val/val_system_5_gpt2.json", 'w') as outfile:
        json.dump(output, outfile, indent=2)

