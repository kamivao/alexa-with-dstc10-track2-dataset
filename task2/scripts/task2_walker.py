import codecs, os
import json



if __name__ == "__main__":
    dataroot = '../data'
    path = os.path.join(os.path.abspath(dataroot))

    with codecs.open(os.path.join(path, "knowledge.json"), 'rb') as fl:
        knowledge = json.load(fl)
    with codecs.open(os.path.join(path, "val/logs.json"), 'rb') as fl2:
        logs = json.load(fl2)

    filename = fl2.name
    with codecs.open(os.path.join(path, "val/labels.json"), 'rb') as fl3:
        labels = json.load(fl3)

    for i in range(0, len(labels)):
        dialogue = logs[i]
        label = labels[i]

        # get ground truth labels for the knowledge-seeking turn
        if label["target"] == True:
            ground_truth_domain = label["knowledge"][0]["domain"]
            ground_truth_ent_id = str(label["knowledge"][0]["entity_id"])
            ground_truth_doc_id = str(label["knowledge"][0]["doc_id"])
            print("Ground truth knowledge: ", ground_truth_domain, ground_truth_ent_id, ground_truth_doc_id)

            # get entity name and docs for the knowledge-seeking turn
            entity_name = knowledge[ground_truth_domain][ground_truth_ent_id]["name"]
            docs = knowledge[ground_truth_domain][ground_truth_ent_id]["docs"]

            # get utterance of the dialogue
            for j in range(0, len(dialogue)):
                utterance = dialogue[j]["text"]
                # do smth with the utterance