import json
import os
import torch
from tqdm import tqdm


def load_model(architecture, task):
    if architecture == 'roberta' and task == 'detection':
        from transformers import RobertaForSequenceClassification, RobertaTokenizer
        model = RobertaForSequenceClassification.from_pretrained("../models/ks_turn_detection/roberta")
        print("Loaded model: ", model)
        tokenizer = RobertaTokenizer.from_pretrained("../models/ks_turn_detection/roberta")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    if architecture == 'roberta_asr' and task == 'detection':
        from transformers import RobertaForSequenceClassification, RobertaTokenizer
        model = RobertaForSequenceClassification.from_pretrained("../models/ks_turn_detection/roberta_asr")
        print("Loaded model: ", model)
        tokenizer = RobertaTokenizer.from_pretrained("../models/ks_turn_detection/roberta_asr")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    if architecture == 'roberta_asr_fillers' and task == 'detection':
        from transformers import RobertaForSequenceClassification, RobertaTokenizer
        model = RobertaForSequenceClassification.from_pretrained("../models/ks_turn_detection/roberta_asr_fillers")
        print("Loaded model: ", model)
        tokenizer = RobertaTokenizer.from_pretrained("../models/ks_turn_detection/roberta_asr_fillers")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    elif architecture == 'bert' and task == 'selection':
        from transformers import AutoModelForSequenceClassification, AutoTokenizer
        model = AutoModelForSequenceClassification.from_pretrained("../models/ent_clf_task2")
        print("Loaded model: ", model)
        tokenizer = AutoTokenizer.from_pretrained("../models/ent_clf_task2")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    elif architecture == 'gpt2' and task == 'selection':
        from transformers import GPT2ForSequenceClassification, GPT2Tokenizer
        model = GPT2ForSequenceClassification.from_pretrained("../models/ent_clf_gpt2_seq_800_hde")

        tokenizer = GPT2Tokenizer.from_pretrained("../models/ent_clf_gpt2_seq_800_hde")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    elif architecture == 'gpt2rl' and task == 'selection':
        from transformers import GPT2ForSequenceClassification, GPT2Tokenizer
        model = GPT2ForSequenceClassification.from_pretrained("../models/gpt2_rl")

        tokenizer = GPT2Tokenizer.from_pretrained("../models/gpt2_rl")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    elif architecture == 'longformer' and task == 'selection':
        from transformers import LongformerForSequenceClassification, LongformerTokenizer
        tokenizer = LongformerTokenizer.from_pretrained("../models/ent_clf_longformer_900")
        print("Loaded tokenizer: ", tokenizer)
        model = LongformerForSequenceClassification.from_pretrained("../models/ent_clf_longformer_900")
        print("Loaded model: ", model)
        return model, tokenizer

    elif architecture == 'gpt2_fillers' and task == 'selection':
        from transformers import GPT2ForSequenceClassification, GPT2Tokenizer
        model = GPT2ForSequenceClassification.from_pretrained("../models/gpt2_es_train5_long_fillers")

        tokenizer = GPT2Tokenizer.from_pretrained("../models/gpt2_es_train5_long_fillers")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    elif architecture == 'roberta_fillers' and task == 'selection':
        from transformers import RobertaForSequenceClassification, RobertaTokenizer
        model = RobertaForSequenceClassification.from_pretrained("../models/roberta_es_5_long_fillers")
        print("Loaded model: ", model)
        tokenizer = RobertaTokenizer.from_pretrained("../models/roberta_es_5_long_fillers")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    elif architecture == 'roberta_large_noisy' and task == 'selection':
        from transformers import RobertaForSequenceClassification, RobertaTokenizer
        model = RobertaForSequenceClassification.from_pretrained("../models/roberta_large_noisy")
        print("Loaded model: ", model)
        tokenizer = RobertaTokenizer.from_pretrained("../models/roberta_large_noisy")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    elif architecture == 'transformerxl' and task == 'domain_classification':
        from transformers import TransfoXLForSequenceClassification, TransfoXLTokenizer
        model = TransfoXLForSequenceClassification.from_pretrained("../models/domain_clf/transformerxl")
        tokenizer = TransfoXLTokenizer.from_pretrained("../models/domain_clf/transformerxl")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    elif architecture == 'roberta' and task == 'domain_classification':
        from transformers import RobertaForSequenceClassification, RobertaTokenizer
        model = RobertaForSequenceClassification.from_pretrained("../models/domain_clf/roberta")
        tokenizer = RobertaTokenizer.from_pretrained("../models/domain_clf/roberta")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer

    elif architecture == 'roberta_large_noisy' and task == 'domain_classification':
        from transformers import RobertaForSequenceClassification, RobertaTokenizer
        model = RobertaForSequenceClassification.from_pretrained("../models/domain_clf/roberta_large_noisy")
        tokenizer = RobertaTokenizer.from_pretrained("../models/domain_clf/roberta_large_noisy")
        print("Loaded tokenizer: ", tokenizer)
        return model, tokenizer


def validate_detected_turn(pred_target, clf_input):
    ks_turn_triggers = ['heat', 'free wifi', 'elevator', 'happy hour', 'noise level', 'do they have t v',
                        'is it a smokin', 'can you check whether they accept', 'good for kid', 'how much is the admission']
    non_ks_turn_triggers = ['confirmation code', 'zip code', 'what type of attraction', 'what type of accomodation', 'confirmation number']
    if pred_target == True:
        for trigger in non_ks_turn_triggers:
            if trigger in clf_input:
                pred_target = False
        return pred_target

    elif pred_target == False:
        for trigger in ks_turn_triggers:
            if trigger in clf_input:
                val_target = True
        return pred_target



def detect_ks_turn(tokenizer, model, seq):
    try:
        inputs = tokenizer(seq, return_tensors="pt")
        outputs = model(**inputs)
        classification_logits = model(**inputs)[0]
        pred_labels = torch.softmax(classification_logits, dim=1).tolist()[0]
        #print("predicted labels: ", pred_labels)
        scores = pred_labels
        print("ks-turn prediction scores, [0] - false, [1] - true", scores)
        if scores[1] > scores[0]:
            return True
        else:
            return False

    except Exception as ex:
        print(ex)



if __name__ == "__main__":

    # load models
    #TODO @Debanjan: if you use any new models, add them to the load_model() function

    # load models for detection (subtask 1),  available models: 'roberta', 'roberta_asr'
    ks_turn_detection_model, ks_turn_detection_tokenizer = load_model(architecture='roberta_asr_fillers', task='detection')


    dname = os.path.dirname(os.getcwd())
    os.chdir(dname)
    print(dname)

    # file 'recovered_logs_all.json' contains asr output after spell-checking in the fields 'recovered_text_t5' and 'recovered_text_symspell'
    with open("data/dstc10_test/test/recovered_logs_all.json", encoding='utf-8') as logs_file:
        logs = json.load(logs_file)

    # ground truth
    with open("data/dstc10_test/test/labels.json", encoding='utf-8') as labels_file:
        labels = json.load(labels_file)

    errors = []
    correct_pred = 0
    for i in tqdm(range(0, len(logs))):
        dialogue = logs[i]
        label = labels[i]


        # # start ks-turn detection
        clf_input = dialogue[-1]["recovered_text_symspell"]
        ground_truth_target = label["target"]
        pred_target = detect_ks_turn(tokenizer=ks_turn_detection_tokenizer, model=ks_turn_detection_model, seq=clf_input)
        print("Knowledge-seeking turn: ", pred_target)
        # validate the predicted target
        val_target = validate_detected_turn(pred_target=pred_target, clf_input=clf_input)


        if ground_truth_target == val_target:
            correct_pred += 1
        else:
            report = {"ground truth: ": ground_truth_target, "predicted target: ": pred_target,
                      "validated target": val_target, "text": clf_input}
            errors.append(report)


    print("KS-turn prediction accuracy: ", correct_pred/len(labels))

    with open("data/dstc10_test/test/errors_t1.json", 'w+') as err_file:
        json_str = json.dumps(errors, indent=2)
        err_file.write(json_str)
        err_file.close()
