# <snippet_imports>
from task2.scripts.relation_predict import predict_relation
import json
import numpy as np
from tqdm import tqdm
# </snippet_imports>
# Created by Debanjan Chaudhuri at 9/20/2021


if __name__ == '__main__':
    valid_data = '../data/test/checked_output_val_set_1.json'
    labels = '../data/test/labels.json'
    logs = '../data/test/logs.json'
    kg_ = '../data/knowledge_relation.json'
    output = '../data/wrong_relations.tsv'

    # load data
    with open(valid_data, 'r') as f:
        valid_data = json.load(f)
    with open(labels, 'r') as f:
        label_dat = json.load(f)
    with open(logs, 'r') as f:
        dial = json.load(f)
    with open(kg_, 'r') as f:
        kg = json.load(f)
    relation_correct = []
    relation_wrong = []
    for j, l in enumerate(tqdm(label_dat)):
        if l['target']:
            domain = l['knowledge'][0]['domain']
            true_ent = l['knowledge'][0]['entity_id']
            correct_relation = l['knowledge'][0]['doc_id']
            relations = [d['relation'] for k, d in kg[domain][str(true_ent)]['docs'].items()]
            # vprint (relations)
            user_utter = dial[j][-1]['text']
            reln_pred, reln_score = predict_relation(user_utter, relations, get_best=True)
            relation_id = [k for k,d in kg[domain][str(true_ent)]['docs'].items() if d['relation'] == reln_pred]
            relation_pred = int(relation_id[0])
            if correct_relation == relation_pred:
                relation_correct.append(1.0)
            else:
                relation_correct.append(0.0)
                relation_wrong.append([user_utter, kg[domain][str(true_ent)]['docs'][str(correct_relation)]['relation'],
                                       kg[domain][str(true_ent)]['docs'][str(relation_pred)]['relation']])

    print ("accuracy: ", np.average(relation_correct))
    with open(output, 'w') as f:
        for r in relation_wrong:
            u, c_r, p_r = r
            f.write(u+'\t'+c_r+'\t'+p_r)
            f.write('\n')


