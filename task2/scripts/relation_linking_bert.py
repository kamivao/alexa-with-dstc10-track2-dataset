# <snippet_imports>
import numpy as np
import pandas as pd
import json
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, recall_score, precision_score, f1_score
import torch
from transformers import TrainingArguments, Trainer
from transformers import BertForSequenceClassification, DistilBertForSequenceClassification
from transformers import BertTokenizerFast, DistilBertTokenizerFast
from transformers import EarlyStoppingCallback
# </snippet_imports>
# Created by Debanjan Chaudhuri at 11/6/2021


# Create torch dataset
class Dataset(torch.utils.data.Dataset):
    def __init__(self, encodings, labels=None):
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx):
        item = {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}
        if self.labels:
            item["labels"] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        return len(self.encodings["input_ids"])


model_name = "distilbert-base-uncased"
# Load trained model
model_path = "../data/rl_model/checkpoint-4000"
# tokenizer = BertTokenizerFast.from_pretrained(model_name)
tokenizer = DistilBertTokenizerFast.from_pretrained(model_name)
model = DistilBertForSequenceClassification.from_pretrained(model_path, num_labels=2)
trainer = Trainer(
    model=model,
    callbacks=[EarlyStoppingCallback(early_stopping_patience=3)],
)
# Define test trainer
test_trainer = Trainer(model)


def predict_reln_bert(query, title_set, get_best=False, top_k=5):
    """
    :param query: the query text
    :param title_set: the set of relations as list
    :param get_best: get the best relation and score, otherwise returns the label probability distribution
    """
    reln_test = []
    for t in title_set:
        reln_test.append(query.lower() + ' [SEP] ' + t.lower())

    tokenized_d = tokenizer(reln_test, padding=True, truncation=True, max_length=512)
    test_dataset = Dataset(tokenized_d)
    raw_pred, _, _ = trainer.predict(test_dataset)
    # Preprocess raw predictions
    # print(raw_pred)
    raw_pred = raw_pred[:, 1]  # get predictions for 1
    # print (raw_pred)
    if get_best:
        y_pred = np.argmax(raw_pred)
        return title_set[y_pred], raw_pred[y_pred]
    else:  # get top k
        ranked_score = np.argsort(-1 * raw_pred)[:top_k]  # -1 for reversing the sort
        # print (ranked_score)
        ranked_reln = [title_set[r] for r in ranked_score]
        reln_score = [raw_pred[r] for r in ranked_score]
        return ranked_reln, reln_score


if __name__ == '__main__':

    text = 'ok uh do they accept a master card'
    # doc = nlp(text)

    # print(doc._.performed_spellCheck)  # Should be True
    # print(doc._.outcome_spellCheck)  # I
    # text_ext = kw_model.extract_keywords(doc._.outcome_spellCheck, keyphrase_ngram_range=(1, 4), use_mmr=True, stop_words=None)
    # print (text_ext[0][0])
    # rel_dist = predict_relation(text_ext[0][0], ['there tv at',
    #                                                                                           'accommodate groups',
    #                                                                                           'you accept apple pay',
    #                                                                                           'guys offer bike parking',
    #                                                                                           'free wifi for customers'],
    #                             get_best=False, top_k=3)
    # print (rel_dist)
    rel_dist = predict_reln_bert(text, ['credit cards', 'there bar or restaurant', 'uggage storage at hotel',
                                       'is street parking available', 'payment options are available', 'is dry cleaning offered'])
    print (rel_dist)

