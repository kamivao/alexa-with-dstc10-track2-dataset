from collections import Counter
from keybert import KeyBERT
from collections import defaultdict
from nltk.corpus import stopwords

import json
import os
# keyword extractor model
kw_model = KeyBERT()

dataroot = '../data'
path = os.path.join(os.path.abspath(dataroot))
# load kg
with open(os.path.join(path, 'knowledge.json'), 'r') as f:
    kg = json.load(f)
stop_en = stopwords.words('english')


# create kg
def create_kg(domain, top_ele):
    """
    get domain and return the top top_ele (int) elements
    the return is in form of a dictionary, where the key is the relation (key element) and values are a list in the form
    of [entity, id, answer]
    """
    _kg = []
    for k, v in kg[domain].items():
        unstruct = v['docs']
        for j, (uk, uv) in enumerate(unstruct.items()):
            kg_key = kw_model.extract_keywords(uv['title'], keyphrase_ngram_range=(2, 4), use_mmr=True, stop_words=None)
            # print (kg_key)
            if kg_key:
                _kg.append([kg_key[0][0], j, v['name'], uv['body']])

    # print(_kg)
    _kg_ele = [l[0] for l in _kg]
    _kg_extracted = Counter(_kg_ele).most_common(top_ele)  # most common relation elements
    _kg_ext_text = [s for s, c in _kg_extracted]
    print (_kg_extracted)
    _kg_ele = defaultdict(list)
    for ele in _kg:
        r, _id, _ent, _obj = ele
        if r in _kg_ext_text:
            _kg_ele[r].append([_ent, _id, _obj])
    return _kg_ele


if __name__ == '__main__':
    hotel_kg = create_kg('hotel', 30)
    print(hotel_kg)
    print(hotel_kg.keys())
