# <snippet_imports>
from transformers import pipeline
from keybert import KeyBERT
# import spacy
# import contextualSpellCheck

# </snippet_imports>
# Created by Debanjan Chaudhuri at 9/11/2021

classifier = pipeline("zero-shot-classification",
                      model="facebook/bart-large-mnli")
# nlp = spacy.load('en_core_web_lg')
# contextualSpellCheck.add_to_pipe(nlp)
""" select spell checkers & load """
# kw_model = KeyBERT()


def predict_relation(query, relation_set, get_best=False, top_k=5):
    """
    :param query: the query text
    :param relation_set: the set of relations as list
    :param get_best: get the best relation and score, otherwise returns the label probability distribution
    """
    # doc = nlp(query)
    # if doc._.performed_spellCheck:  # if spell check is done
    #     query_sc = doc._.outcome_spellCheck
    # else:
    #     query_sc = query
    # print (query_sc)
    # query_kw = kw_model.extract_keywords(query, keyphrase_ngram_range=(1, 4), use_mmr=True, stop_words=None)
    # query_sc = query_kw[0][0]
    if 'ummm' in query: # remove umm and umms
        query = query.split('umm')[-1].replace('and', '')
    elif 'umm' in query:
        query = query.split('umm')[-1].replace('and', '')
    # print (query)
    reln_predict = classifier(query, relation_set, multi_label=True)
    # print (reln_predict)

    if get_best:
        return reln_predict['labels'][0], reln_predict['scores'][0]
    else:
        return reln_predict['labels'][:top_k], reln_predict['scores'][:top_k]


if __name__ == '__main__':
    kw_model = KeyBERT()
    text = 'ok uh do they accept a master card'
    # doc = nlp(text)

    # print(doc._.performed_spellCheck)  # Should be True
    # print(doc._.outcome_spellCheck)  # I
    # text_ext = kw_model.extract_keywords(doc._.outcome_spellCheck, keyphrase_ngram_range=(1, 4), use_mmr=True, stop_words=None)
    # print (text_ext[0][0])
    # rel_dist = predict_relation(text_ext[0][0], ['there tv at',
    #                                                                                           'accommodate groups',
    #                                                                                           'you accept apple pay',
    #                                                                                           'guys offer bike parking',
    #                                                                                           'free wifi for customers'],
    #                             get_best=False, top_k=3)
    # print (rel_dist)
    rel_dist = predict_relation(text, ['credit cards', 'there bar or restaurant', 'uggage storage at hotel',
                                       'is street parking available', 'payment options are available', 'is dry cleaning offered'])
    print (rel_dist)